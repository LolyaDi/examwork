﻿using System.Threading.Tasks;

namespace ExamWorkWPF
{
    public interface IDownloader
    {
        Task<string> Download(string url, string filePath);
    }
}
