﻿using System.Threading;
using System.Windows;

namespace ExamWorkWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private FileService _fileService;
        private int[] _numbers;
        private int _increment;

        public MainWindow()
        {
            InitializeComponent();
            _fileService = new FileService();
        }

        private void Load(bool isLoading)
        {
            if (isLoading)
            {
                progressBarResult.Visibility = Visibility.Visible;
                progressResultText.Text = "Loading...";
            }
            else
            {
                progressBarResult.Visibility = Visibility.Collapsed;
                progressResultText.Text = "Done.";
            }
        }

        private async void DownloadFileButtonClick(object sender, RoutedEventArgs e)
        {
            if (fileUrl.Text == "" || filePath.Text == "")
            {
                MessageBox.Show("Complete a form!");
                return;
            }
            else if (!_fileService.CheckFilePath(filePath.Text))
            {
                MessageBox.Show("Check your file path!");
                return;
            }

            var file = new File
            {
                Name = filePath.Text.Substring(filePath.Text.LastIndexOf(@"\")),
                Path = filePath.Text
            };

            Load(true);

            using (var context = new DataContext())
            {
                context.Files.Add(file);
                await context.SaveChangesAsync();
            }

            var result = await _fileService.Download(fileUrl.Text, filePath.Text);

            Load(false);

            MessageBox.Show(result);
        }

        private void StartButtonClick(object sender, RoutedEventArgs e)
        {
            _increment = 0;

            if (int.TryParse(countText.Text, out int count))
            {
                _numbers = new int[count];

                for (int i = 0; i < count; i++)
                {
                    ThreadPool.QueueUserWorkItem((number) =>
                    {
                        Thread.Sleep(100);
                        _numbers[_increment] = (int)number;
                        Interlocked.Increment(ref _increment);
                    }, i + 1);
                }
            }
            else
            {
                MessageBox.Show("Please enter a valid number!");
                return;
            }
        }
    }
}
